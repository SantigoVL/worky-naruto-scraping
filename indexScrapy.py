import scrapy
import scrapingCharacterFunctions as charScraping
import datetime

class QuotesSpider(scrapy.Spider):
    name = "quotes"
    start_urls = [
        'https://naruto.fandom.com/wiki/Category:Characters',
    ]

    charactersUrls = []

    def processCharactersUrls(self, urls):
        print("[Starting process. Time: {}]".format(datetime.datetime.now()))
        for charUrl in urls:
            data = ''
            #save url and/or get url id
            urlID = charScraping.saveUniqueUrlGetID(charUrl)
            try:
                data = charScraping.getCharDataFromUrl(charUrl)
                try:
                    if(not charScraping.ifCharExistByUrl(charUrl)):
                        try:
                            insertId = charScraping.saveToMongoDB(data).inserted_id
                            print("Character saved with the id: {}".format(insertId))
                        except:
                            print("Error at saving new character")
                    else:
                        if(charScraping.updateCharByUrl(charUrl, data) > 0):
                            print("Character update at url:{}".format(charUrl))
                        print("Up to date: {}".format(charUrl))    
                except:
                    print("An error oucurred when trying DB operation")
            except:
                print("The url {} is not compatible with the current scrapping script".format(charUrl))

    def parse(self, response):
        siteUrl = "https://naruto.fandom.com"
        for character in response.css('li.category-page__member'):
            self.charactersUrls.append(siteUrl + (character.css('a.category-page__member-link::attr("href")').extract_first()))

        next_page = response.css('a.category-page__pagination-next::attr("href")').extract_first()
        if next_page is not None:
            yield response.follow(next_page, self.parse)
        else:
            self.processCharactersUrls(self.charactersUrls)