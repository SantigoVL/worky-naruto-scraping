from lxml import etree
from bs4 import BeautifulSoup
from bson.objectid import ObjectId
import urllib.request
import pymongo


#Intialize Mongo DB connection
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["CHARACTERS"]

## Helper functions

# cleanText(text:string) : string
def cleanText(text):
    text = text.strip()
    text = text.rstrip()
    return text.replace("\n", "")

# saveToFile(data:json) : null
def saveToFile(data):
    fn = open("charData.json","a+")
    fn.write(data)
    fn.close()

# saveToMongoDB(data:dictionary) : MongoCursor
def saveToMongoDB(data):
    mycol = mydb["personajes"]
    return mycol.insert_one(data)

# saveUniqueUrlGetID(url:string) : string 
def saveUniqueUrlGetID(url):
    mycol = mydb["direcciones"]
    urlIdStr = ''
    try:
        prevRegistry = mycol.find({"url": url})
        urlIdStr =  prevRegistry["_id"]
    except:
        urlIdStr =  mycol.insert_one({"url": url, "status": "1"}).inserted_id
    return str(urlIdStr)

# updateCharByUrl(url:string, charData:dictionary) : int
def updateCharByUrl(url, charData):
    mycol = mydb["personajes"]
    try:
        return  mycol.update_one({"sourceUrl": url}, {"$set": charData}).modified_count
    except:
        print("Error at update char data")

# ifCharExistByUrl(url:string) : bool
def ifCharExistByUrl(url):
    mycol = mydb["personajes"]
    count =  mycol.count_documents({"sourceUrl": url})
    return True if count > 0 else False

# dictionaryKeysToLowercase(dict:dictionary) : dictonary
def dictionaryKeysToLowercase(dict):
    return {k.lower(): v for k, v in dict.items()}
       
# getCharDataFromUrl(url:string) : dictionary 
def getCharDataFromUrl(url):

    response = urllib.request.urlopen(url)
    pageData = response.read()

    #f = open(url "r", encoding = 'utf-8')
    #pageData = f.read()
    soup = BeautifulSoup(pageData, "lxml")
    table = etree.XML(str(soup.table))
    soup.table.clear()
    article = etree.XML(str(soup.article))

    #Initialize data structure
    charDataStructure = {}

    #Process article content
    sections = {}
    curSection = 'Description'
    sections[curSection] = ''

    for element in article.iter("h2", "li", "p", "figure"):
        if(element.tag == 'h2'):
            curSection = cleanText(' '.join(element.itertext()))
            sections[curSection] = ''
        else:
            sections[curSection] += cleanText(' '.join(element.itertext()))

    #Process table content
    tableSections = {}
    curSection = ''
    subsection = {}
    curSubection = ''
    iCount = 0

    for element in table.iter("th", "td", "li"):
        if(iCount == 2): #Name
            tableSections['fullName'] = cleanText(' '.join(element.itertext()))
        if(iCount >= 3): #Section and contents
            if(element.tag == 'th' and element.cssselect('th.mainheader') != []):
                subsection = {}
                curSection = cleanText(' '.join(element.itertext()))
                tableSections[curSection] = ''
            elif(element.tag == 'th' and element.cssselect('th.mainheader') == []):
                curSubection = cleanText(' '.join(element.itertext())).lower()
                subsection[curSubection] = ''
            elif(element.tag == 'td'):
                if(not curSubection in subsection):
                    break
                subsection[curSubection] += cleanText(' '.join(element.itertext()))
            tableSections[curSection] = subsection
        iCount += 1

    #Final data structure to json
    charDataStructure['name'] = soup.find_all("h1", class_="page-header__title")[0].get_text()
    charDataStructure["sourceUrl"] = url
    charDataStructure['imageUrl'] = table.xpath("//td/a/img/@data-src")[0]
    charDataStructure['tableData'] = dictionaryKeysToLowercase(tableSections)
    charDataStructure['bodyData'] = dictionaryKeysToLowercase(sections)
    #return json.dumps(tableSections)
    return charDataStructure