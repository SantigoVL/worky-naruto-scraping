import scrapy


class QuotesSpider(scrapy.Spider):
    name = "quotes"
    
    start_urls = [
        'https://naruto.fandom.com/wiki/Category:Characters',
    ]

    def parse(self, response):
        siteUrl = "https://naruto.fandom.com"
        for character in response.css('li.category-page__member'):
            yield {
                'url': siteUrl + (character.css('a.category-page__member-link::attr("href")').extract_first()),
            }

        next_page = response.css('a.category-page__pagination-next::attr("href")').extract_first()
        if next_page is not None:
            yield response.follow(next_page, self.parse)